<?xml version="1.0" encoding="UTF-8" ?>
<Package name="story_game" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/brass-band" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/bravest" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/cardinal" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/defeated" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/fierce-duel" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/crow_sounds" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/dragon_fire_sounds" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/tavern_sounds" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/mood_fear" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/mystical" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/crickets" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/game_over_sad" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/gunshot" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/scary_forest" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/wedding" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/bird_sounds" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/hypno" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/BothArmsBump_LeanRight_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/BothArmsUpAndDown_HeadShake_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Chill" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Curious" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Disco" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/DoubleHorizontalBumpRightArm_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Excited" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/FancyRightArmCircle_LeanRight_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/FastPointAtUsersLeftArm_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Fear" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/GoToStance_Exclamation_Center" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/GoToStance_Exclamation_Flex" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/GoToStance_Question_Center" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/GoToStance_Question_Flex" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/GoToStance_Question_LeanFront" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Hey_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Kisses" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/LittleArmsBumpInFront_LeanRight01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/LittleArmsBump_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/LittleSpreadRightArm_HeadShake_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/OfferBothHands_HeadNod_LeanLeft_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/SlowlyOfferBothHands_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/Thinking" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/WideBothArmsCircle_LeanLeft_01" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="animations/YouKnowWhat_1" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/angry_sheriff" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/black_screen" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/castle" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/crossroad" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/death" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/dragon_fall" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/dragon_fire" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/dragon_meet" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/dragon_ride" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/dragon_touch" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/end_game" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/fall" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/gandalf" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/happy_sheriff" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/high_pepper" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/marriage" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/scary_forest" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/sign" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/sky_pepper" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/start_forest" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/tavern" xar="behavior.xar" />
        <BehaviorDescription name="behavior" src="images_anims/village" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="dlg_story_game" src="dlg_story_game/dlg_story_game.dlg" />
    </Dialogs>
    <Resources>
        <File name="Chevaux_sur_paves3" src="sounds/Chevaux_sur_paves3.ogg" />
        <File name="Medieval_Fanfare" src="sounds/Medieval_Fanfare.ogg" />
        <File name="applause2" src="sounds/applause2.ogg" />
        <File name="sarcasticLaught6" src="sounds/sarcasticLaught6.ogg" />
        <File name="sword_fight_mix" src="sounds/sword_fight_mix.ogg" />
        <File name="high_pepper" src="images/high_pepper.png" />
        <File name="choice_sentences" src="Aldebaran/choice_sentences.xml" />
        <File name="dragon_fire" src="images/dragon_fire.jpg" />
        <File name="dragon_meet" src="images/dragon_meet.jpg" />
        <File name="dragon_ride" src="images/dragon_ride.jpg" />
        <File name="dragon_touch" src="images/dragon_touch.png" />
        <File name="scary_forest" src="images/scary_forest.jpg" />
        <File name="start_forest" src="images/start_forest.png" />
        <File name="tavern" src="images/tavern.png" />
        <File name="village" src="images/village.png" />
        <File name="sky_pepper" src="images/sky_pepper.jpg" />
        <File name="angry_sheriff" src="images/angry_sheriff.jpg" />
        <File name="black_screen" src="images/black_screen.jpg" />
        <File name="castle" src="images/castle.png" />
        <File name="crossroad" src="images/crossroad.jpg" />
        <File name="death" src="images/death.jpg" />
        <File name="happy_sheriff" src="images/happy_sheriff.jpg" />
        <File name="sign" src="images/sign.png" />
        <File name="end_game" src="images/end_game.jpg" />
        <File name="marriage" src="images/marriage.jpg" />
        <File name="dragon_fall" src="images/dragon_fall.jpg" />
        <File name="epicsax" src="epicsax.ogg" />
        <File name="heaven1" src="behavior_1/heaven1.ogg" />
        <File name="gandalf" src="images/gandalf.jpg" />
        <File name="surprise3" src="animations/mood_fear/surprise3.ogg" />
        <File name="heaven1" src="animations/mystical/behavior_1/heaven1.ogg" />
        <File name="fall" src="images/fall.jpg" />
        <File name="birds" src="sounds/birds.wav" />
        <File name="crickets" src="sounds/crickets.wav" />
        <File name="fire" src="sounds/fire.wav" />
        <File name="game_over_sad" src="sounds/game_over_sad.wav" />
        <File name="gunshot" src="sounds/gunshot.mp3" />
        <File name="hypno" src="sounds/hypno.wav" />
        <File name="raven" src="sounds/raven.mp3" />
        <File name="scary_forest" src="sounds/scary_forest.wav" />
        <File name="wedding" src="sounds/wedding.wav" />
        <File name="surprise3" src="animations/Fear/surprise3.ogg" />
        <File name="tavern" src="sounds/tavern.mp3" />
        <File name="angry_sheriff" src="html/angry_sheriff.jpg" />
        <File name="black_screen" src="html/black_screen.jpg" />
        <File name="castle" src="html/castle.png" />
        <File name="crossroad" src="html/crossroad.jpg" />
        <File name="death" src="html/death.jpg" />
        <File name="dragon_fall" src="html/dragon_fall.jpg" />
        <File name="dragon_fire" src="html/dragon_fire.jpg" />
        <File name="dragon_meet" src="html/dragon_meet.jpg" />
        <File name="dragon_ride" src="html/dragon_ride.jpg" />
        <File name="dragon_touch" src="html/dragon_touch.png" />
        <File name="end_game" src="html/end_game.jpg" />
        <File name="fall" src="html/fall.jpg" />
        <File name="gandalf" src="html/gandalf.jpg" />
        <File name="happy_sheriff" src="html/happy_sheriff.jpg" />
        <File name="high_pepper" src="html/high_pepper.png" />
        <File name="marriage" src="html/marriage.jpg" />
        <File name="scary_forest" src="html/scary_forest.jpg" />
        <File name="sign" src="html/sign.png" />
        <File name="sky_pepper" src="html/sky_pepper.jpg" />
        <File name="start_forest" src="html/start_forest.png" />
        <File name="tavern" src="html/tavern.png" />
        <File name="village" src="html/village.png" />
        <File name="Autori" src="Autori.txt" />
        <File name="gun" src="sounds/gun.mp3" />
        <File name="style" src="html/css/style.css" />
        <File name="index" src="html/index.html" />
        <File name="jquery-1.11.0.min" src="html/js/jquery-1.11.0.min.js" />
        <File name="main" src="html/js/main.js" />
        <File name="robotutils" src="html/js/robotutils.js" />
    </Resources>
    <Topics>
        <Topic name="dlg_story_game" src="dlg_story_game/dlg_story_game_czc.top" topicName="dlg_story_game" language="cs_CZ" />
        <Topic name="dlg_story_game" src="dlg_story_game/dlg_story_game_enu.top" topicName="dlg_story_game" language="en_US" />
    </Topics>
    <IgnoredPaths>
        <Path src="images/resize.sh" />
        <Path src="translations/translation_cs_CZ.qm" />
        <Path src="translations/translation_en_US.qm" />
    </IgnoredPaths>
    <Translations auto-fill="en_US">
        <Translation name="translation_cs_CZ" src="translations/translation_cs_CZ.ts" language="cs_CZ" />
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
