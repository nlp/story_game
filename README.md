# Dialog for Pepper to play the Story Game

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/projects/pepper)

The application is to be installed from the `story_game-0.0.12.pkg` file.

The application contains the game dialog in English and Czech.

Start with "Chci si zahrát hru / I want to play game".

See [an example game video](https://nlp.fi.muni.cz/projects/pepper/videos/pepper-story_game.mkv)

## Installation

* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the package as usual for the Pepper robot
