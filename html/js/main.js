var PRESENTATION_VARIABLE = "story_img";

function update_variable(stateJson) {
//console.log('update variable', stateJson)
    //var state = JSON.parse(stateJson);
    var img_name = stateJson;
    if (img_name != undefined) {
      $('.image').hide();
      $('#image_'+img_name).show();
    }
}

function onFrontTactilTouched(value)
{
    console.log("FrontTactilTouched event", value);
    if (value) document.getElementById("touch").innerHTML = "touched";
    else document.getElementById("touch").innerHTML = "Not touched";
}

var application = function(){
    RobotUtils.onService(function (ALMemory) {
	ALMemory.getData(PRESENTATION_VARIABLE).then(update_variable);
	update_variable('1');
    });
    RobotUtils.subscribeToALMemoryEvent(PRESENTATION_VARIABLE, update_variable);
    RobotUtils.subscribeToALMemoryEvent("FrontTactilTouched", onFrontTactilTouched);
    //RobotUtils.onService(function (ALSpeechRecognition) {
    //    ALSpeechRecognition.subscribe('dialog_subtitles').then(function() {
    //        console.log("ALSpeechRecognition subscribed");
    //    });
    //}, function() {
    //    console.log("Failed to subscribe to ALSpeechRecognition")
    //});
    ////RobotUtils.subscribeToALMemoryEvent("FrontTactilTouched", onFrontTactilTouched);
    //RobotUtils.subscribeToALMemoryEvent("SpeechDetected", onSpeechDetected);
    ////RobotUtils.subscribeToALMemoryEvent("ALSpeechRecognition/Status", onSpeechStatus);
    //RobotUtils.subscribeToALMemoryEvent("Dialog/IsStarted", onSpeechStatus);
    ////RobotUtils.subscribeToALMemoryEvent("ALSpeechRecognition/ActiveListening", onSpeechStatus);
    ////RobotUtils.subscribeToALMemoryEvent("WordRecognized", onWordRecognized);
    ////RobotUtils.subscribeToALMemoryEvent("WordRecognizedAndGrammar", onWordRecognized);
    ////RobotUtils.subscribeToALMemoryEvent("Dialog/LastInput", onLastInput);
    ////RobotUtils.subscribeToALMemoryEvent("Dialog/Answered", onAnswered);
    //RobotUtils.subscribeToALMemoryEvent("WordRecognizedAndGrammar", onLastInput);
    //RobotUtils.subscribeToALMemoryEvent("ALTextToSpeech/CurrentSentence", onAnswered);
    //RobotUtils.subscribeToALMemoryEvent("Dialog/SwitchLanguage", onLanguageTTS);
}

/*
var KEY_TABLETSETATE = "dialog_subtitles/TabletState";

function onFrontTactilTouched(value)
{
    console.log("FrontTactilTouched event", value);
    if (value) document.getElementById("touch").innerHTML = "touched";
    else document.getElementById("touch").innerHTML = "Not touched";
}

function onSpeechDetected(value)
{
    console.log("SpeechDetected event", value);
    if (value) {
	$("#speech").html("Speech detected");
	$("#speech").removeClass("inactive");
	$("#speech").addClass("active");
    }
    else {
	$("#speech").html("Speech not detected");
	$("#speech").removeClass("active");
	$("#speech").addClass("inactive");
    }
}

function onWordRecognized(value)
{
    //console.log("WordRecognized event", value);
    console.log("WordRecognizedAndGrammar event", value);
    document.getElementById("word").innerHTML = value;
}

function onSpeechStatus(value)
{
    console.log("ALSpeechRecognition/Status event", value);
    document.getElementById("status").innerHTML = value;
}

var application = function(){
    RobotUtils.onService(function (ALDialogSubtitles) {
        $("#noservice").hide();
        ALDialogSubtitles.get().then(function(level) {
            // Find the button with the right level:
            $(".levelbutton").each(function() {
                var button = $(this);
                if (button.data("level") == level) {
                    button.addClass("highlighted");
                    button.addClass("clicked");
                }
            });
            // ... and show all buttons:
            $("#buttons").show();
        });
        $(".levelbutton").click(function() {
            // grey out the button, until we hear back that the click worked.
            var button = $(this);
            var level = button.data("level");
            $(".levelbutton").removeClass("highlighted");
            $(".levelbutton").removeClass("clicked");
            button.addClass("clicked");
            ALDialogSubtitles.set(level).then(function(){
                button.addClass("highlighted");
            });
        })
    }, function() {
        console.log("Failed to get the service.")
        $("#noservice").show();
    });
    RobotUtils.onService(function(ALTextToSpeech) {
        // Bind button callbacks
        $(".bleeper").click(function() {
            ALTextToSpeech.say($(this).html());
        });
    });
    RobotUtils.subscribeToALMemoryEvent("FrontTactilTouched", onFrontTactilTouched);
    RobotUtils.subscribeToALMemoryEvent("SpeechDetected", onSpeechDetected);
    RobotUtils.subscribeToALMemoryEvent("ALSpeechRecognition/Status", onSpeechStatus);
    //RobotUtils.subscribeToALMemoryEvent("WordRecognized", onWordRecognized);
    RobotUtils.subscribeToALMemoryEvent("WordRecognizedAndGrammar", onWordRecognized);
    RobotUtils.onService(function (ALMemory) {
	ALMemory.getData(KEY_TABLETSETATE).then(updateTabletState);
	//updateTabletState('{"title": "TEST"}');
    });
    RobotUtils.subscribeToALMemoryEvent(KEY_TABLETSETATE, updateTabletState);
 */
